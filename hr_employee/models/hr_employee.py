from odoo import api, fields, models
from datetime import datetime
import requests


class HrEmployee(models.Model):
    _inherit = "hr.employee"

    def birthday_reminder(self):
        current_date = datetime.now().date()
        current_month = current_date.month

        employees = self.search([("birthday", "!=", False,)])
        main_message = f""

        for employee in employees:
            message = f"Доброго утра, команда! С первым днём нового месяца! А вот и наши именинники:{employee.name} - {employee.birthday} \n"
            main_message += message

        bot_token = self.env['ir.config_parameter'].sudo().get_param('hr.chat_bot_token')
        chat_id = self.env['ir.config_parameter'].sudo().get_param('hr.messenger_chat_id')
        url = f"https://api.telegram.org/bot{bot_token}/sendMessage?chat_id={chat_id}&text={main_message}"
        if employee.birthday.month == current_month:
            print(requests.get(url).json())
