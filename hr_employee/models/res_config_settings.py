from odoo import fields, models, api


class ResConfigSettings(models.TransientModel):
    _inherit = "res.config.settings"

    chat_bot_token = fields.Char(string="ChatBot Token", config_parameter="hr.chat_bot_token")
    messenger_chat_id = fields.Char(string="Chat id", config_parameter="hr.messenger_chat_id")
