{
    'name': 'Icode employee birthdays',
    'summary': 'Employee birthdays',
    'version': '16.0',
    'description': """ CRM contracts """,
    'depends': ["hr"],
    'author': 'Artyom',
    'license': 'OPL-1',
    'website': 'https://icode.by',
    'data': [
        "data/birthday_cron.xml",
        "views/res_config_settings_views.xml",
    ],
}
